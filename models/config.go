package models

type PriceNew struct {
	Prices []struct {
		Type        string `json:"type"`
		Name        string `json:"name"`
		PricesArray []struct {
			ID    string  `json:"id"`
			Price float64 `json:"price"`
		} `json:"prices_array"`
	} `json:"prices"`
}
