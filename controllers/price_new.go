package controllers

import (
	// Import the Gin library
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/oartemyev/timpricesvc/models"
	"gitlab.com/oartemyev/timpricesvc/pkg/logging"
)

type PriceNewController struct{}

func (h *PriceNewController) Run(c *gin.Context) {
	var (
		data    models.PriceNew
		InfoLog *log.Logger
		//		ErrLog  *log.Logger
	)
	InfoLog = logging.GetInfoLog()
	//	ErrLog = logging.GetErrorLog()

	if c.BindJSON(&data) != nil {
		// specified response
		c.JSON(406, gin.H{"message": "Provide relevant fields"})
		// abort the request
		c.Abort()
		// return nothing
		return
	}

	nShop := 0
	nPrice := 0
	for i := 0; i < len(data.Prices); i++ {
		nShop++
		for j := 0; j < len(data.Prices[i].PricesArray); j++ {
			nPrice++
		}
	}
	s := fmt.Sprintf("Accepted %d prices for %d shops", nPrice, nShop)
	InfoLog.Println(s)

	c.JSON(201, gin.H{"message": s})
}
