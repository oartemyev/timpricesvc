package logging

import (
	"fmt"
	"io"
	"log"
	"os"
)

var infolog *log.Logger
var errlog *log.Logger

func GetInfoLog() *log.Logger {
	return infolog
}

func GetErrorLog() *log.Logger {
	return errlog
}

func InitLog(o io.Writer) {

	//	infolog = log.New(o, "INFO\t", log.LstdFlags|log.Lmsgprefix)
	//	errlog = log.New(o, "ERROR\t", log.LstdFlags|log.Lmsgprefix)
	infolog = log.New(o, "[INFO]\t", log.LstdFlags)
	errlog = log.New(o, "[ERROR]\t", log.LstdFlags)

}

func Init() {
	err := os.MkdirAll("logs", 0755)
	if err != nil || os.IsExist(err) {
		panic("can't create log dir. no configured logging to files")
	} else {
		allFile, err := os.OpenFile("logs/all.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0660)
		if err != nil {
			panic(fmt.Sprintf("[Error]: %s", err))
		}
		InitLog(allFile)
	}
}
