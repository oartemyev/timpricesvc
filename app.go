package main

import (
	"fmt"
	"io"
	"os"

	"log"

	"gitlab.com/oartemyev/timpricesvc/controllers"
	"gitlab.com/oartemyev/timpricesvc/pkg/logging"

	// Import godotenv for .env variables
	"github.com/joho/godotenv"
	// Import gin for route definition
	"github.com/gin-gonic/gin"
)

var (
	InfoLog *log.Logger
	ErrLog  *log.Logger
)

// init gets called before the main function
func init() {
	// Log error if .env file does not exist
	if err := godotenv.Load(); err != nil {
		log.Printf("No .env file found")
	}

	err := os.MkdirAll("logs", 0755)
	if err != nil || os.IsExist(err) {
		fmt.Println(err)
		panic("can't create log dir. no configured logging to files")
	}
}

func InitLog() {

	gin.SetMode(gin.ReleaseMode)
	// Disable Console Color, you don't need console color when writing the logs to file.
	gin.DisableConsoleColor()
	f, _ := os.OpenFile("logs/gin.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0660)
	gin.DefaultWriter = io.MultiWriter(f)
	//	defer f.Close()

	//logging.Init()
	logging.InitLog(f)
	InfoLog = logging.GetInfoLog()
	ErrLog = logging.GetErrorLog()
	InfoLog.Println("logger initialized")

}

func main() {

	InitLog()

	// Init gin router
	router := gin.Default()

	v1 := router.Group("/api/v1")
	{
		// Define the hello controller
		hello := new(controllers.PriceNewController)
		// Define a GET request to call the Default
		// method in controllers/hello.go
		v1.POST("/new-price", hello.Run)
	}

	// Handle error response when a route is not defined
	router.NoRoute(func(c *gin.Context) {
		// In gin this is how you return a JSON response
		c.JSON(404, gin.H{"message": "Not found"})
	})

	// Init our server
	//	router.Run(":9000")
//	router.RunTLS(":9000", "cert/cert.pem", "cert/key.pem")
	router.RunTLS(":9000", "cert/localhost.crt", "cert/localhost.key")

}
